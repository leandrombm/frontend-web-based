﻿using AutoMapper;
using CRUDProject.Application.ViewModel;
using CRUDProject.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CRUDProject.Application.Imp.AutoMapper
{
    public class DomainToViewModelCliente : Profile
    {
        public override string ProfileName
        {
            get
            {
                return "DomainToViewModelCliente";
            }
        }
        protected override void Configure()
        {
            Mapper.CreateMap<Cliente, ClienteViewModel>();
            
        }
    }
}
