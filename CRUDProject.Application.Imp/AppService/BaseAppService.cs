﻿using AutoMapper;
using CRUDProject.Application.Interfaces;
using CRUDProject.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUDProject.Application.Imp.AppService
{
    public class BaseAppService<TEntity, TEntityViewModel> : IBaseAppService<TEntityViewModel> where TEntity : class
    {
        private readonly IBaseRepository<TEntity> _baseRepository;
        public BaseAppService(IBaseRepository<TEntity> baseRepository)
        {
            _baseRepository = baseRepository;
        }
        public virtual void Add(TEntityViewModel entityViewModel)
        {
            var entity = Mapper.Map<TEntity>(entityViewModel);
            _baseRepository.Add(entity);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public virtual IEnumerable<TEntityViewModel> GetAll()
        {
            return Mapper.Map<IEnumerable<TEntityViewModel>>(_baseRepository.GetAll());
        }

        public virtual TEntityViewModel GetById(int id)
        {
            return Mapper.Map<TEntityViewModel>(_baseRepository.GetById(id));
        }

        public virtual void Remove(int id)
        {
            var entity = _baseRepository.GetById(id);
            _baseRepository.Remove(entity);
        }

        public virtual void Update(TEntityViewModel entityViewModel)
        {
            var entity = Mapper.Map<TEntity>(entityViewModel);
            _baseRepository.Update(entity);
        }
    }
}
