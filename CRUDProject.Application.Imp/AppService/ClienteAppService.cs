﻿using AutoMapper;
using CRUDProject.Application.Interfaces;
using CRUDProject.Application.ViewModel;
using CRUDProject.Domain.Entities;
using CRUDProject.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUDProject.Application.Imp.AppService
{
    public class ClienteAppService : IClienteAppService
    {
        private readonly IClienteRepository _clienteRepository;
        public ClienteAppService(IClienteRepository clienteRepository)
        {
            _clienteRepository = clienteRepository;
        }
        public void Add(ClienteViewModel entityViewModel)
        {
            var entity = Mapper.Map<Cliente>(entityViewModel);
            _clienteRepository.Add(entity);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ClienteViewModel> GetAll()
        {
            return Mapper.Map<IEnumerable<ClienteViewModel>>(_clienteRepository.GetAll());
        }

        public ClienteViewModel GetById(int id)
        {
            return Mapper.Map<ClienteViewModel>(_clienteRepository.GetById(id));
        }

        public void Remove(int id)
        {
            var cliente = _clienteRepository.GetById(id);
            _clienteRepository.Remove(cliente);
        }

        public void Update(ClienteViewModel entityViewModel)
        {
            var entity = Mapper.Map<Cliente>(entityViewModel);
            _clienteRepository.Update(entity);
        }
    }
}
