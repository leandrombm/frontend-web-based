﻿function ClienteViewModel(response) {
    var self = this;

    self.Id = ko.observable(!response ? 0 : response.Id);
    self.NomeCliente = ko.observable(!response ? "" : response.NomeCliente);
    self.Idade = ko.observable(!response ? "" : response.Idade);
    self.Endereco = ko.observable(!response ? "" : response.Endereco);
    self.Profissao = ko.observable(!response ? "" : response.Profissao);
    self.Sexo = ko.observable(!response ? "" : response.Sexo);
}