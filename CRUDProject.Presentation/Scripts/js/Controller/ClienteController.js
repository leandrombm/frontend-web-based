﻿function SPAController() {
    var self = this;
    self.Clientes = ko.observableArray([]);
    self.ClienteDetails = ko.observable(null);
    self.ClienteCreate = ko.observable(null);
    self.ClienteEdit = ko.observable(null);
    self.RelatorioCliente = ko.observableArray([]);
    self.Router = ko.observable("");

    self.Create = function () {
        var jsonCliente = ko.toJS(self.ClienteCreate);
        $.post("/Home/Create/", { clienteViewModel: jsonCliente }, function (data) {
            if (data == 200) {
                alert("Cliente Cadastrado com Sucesso!");


                self.GoTo("Clientes");
            }
        })
    }

    self.Edit = function () {
        var jsonClienteEdit = ko.toJS(self.ClienteEdit);
        $.ajax({
            url: "/Home/Edit/",
            type: 'PATCH',
            data: jsonClienteEdit,
            success: function (data) {
                if (data == 200) {
                    alert("Cliente Editado com Sucesso!");
                    self.GoTo("Clientes");
                }
            }
        });
    }

    self.GoTo = function (route) {
        self.Router(route)
        location.hash = self.Router();
    }
    self.GoToDetails = function (route) {
        self.Router('Details/' + route.Id());
        location.hash = self.Router();
    }
    self.GoToEdit = function (route) {
        self.Router('Edit/' + route.Id());
        location.hash = self.Router();
    }
    self.Remove = function (context) {
        $.ajax({
            url: "/Home/Delete/" + context.Id(),
            type: 'DELETE',
            success: function (data) {
                if (data == 200) {
                    alert("Cliente Removido com Sucesso!");
                    var item = ko.utils.arrayFirst(self.Clientes(), function (item) {
                        return item.Id() == context.Id();
                    })

                    self.Clientes.remove(item);
                }
            }
        });
    }
    Sammy(function () {
        this.get("#Clientes", function () {
            var router = this.params.Router;
            self.Clientes([]);
            self.ClienteDetails(null)
            self.ClienteCreate(null)
            self.ClienteEdit(null)
            self.RelatorioCliente(null)
            $.get("/Home/GetAll", function (data) {


                for (var i = 0; i < data.length; i++) {
                    self.Clientes.push(new ClienteViewModel(data[i]));
                }

            });
        });
        this.get("#Relatorios", function () {
            var router = this.params.Router;
            self.Clientes(null);
            self.ClienteDetails(null)
            self.ClienteCreate(null)
            self.ClienteEdit(null)
            self.RelatorioCliente([])
            $.get("/Home/GetAll", function (data) {

                for (var i = 0; i < data.length; i++) {
                    self.RelatorioCliente.push(new ClienteViewModel(data[i]));
                }
                ChartController(self.RelatorioCliente);

            });
        });
        this.get("#:Router/:Id", function () {
            self.Clientes([]);
            self.ClienteDetails(null)
            self.ClienteCreate(null)
            self.ClienteEdit(null)
            self.RelatorioCliente(null)
            var router = this.params.Router;
            $.get("/Home/Details/" + this.params.Id, function (data) {
                if (router == "Details")
                    self.ClienteDetails(new ClienteViewModel(data));
                else
                    self.ClienteEdit(new ClienteViewModel(data));

            });
        });



        this.get("#Create", function () {
            self.Clientes([]);
            self.ClienteDetails(null)
            self.ClienteEdit(null)
            self.RelatorioCliente(null)
            self.ClienteCreate(new ClienteViewModel())


        })
        this.get("", function () {
            self.Clientes([]);
            self.ClienteEdit(null)
            self.ClienteDetails(null)
            self.ClienteCreate(null)
            self.RelatorioCliente(null)


        })
    }).run();

}

var _spaController
$(document).ready(function () {

    _spaController = new SPAController();

    ko.applyBindings(_spaController)
})


