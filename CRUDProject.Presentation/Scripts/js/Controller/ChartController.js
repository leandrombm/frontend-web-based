﻿function ChartController(clientes) {

    var result = TratarDados(clientes)

    var ctx = $("#chart-idade")[0].getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["0 a 9", "10 a 19", "20 a 29", "30 a 39", "Maior que 40"],
            datasets: [{
                label: 'Faixa de idade',
                data: result[0],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
    var ctx = $("#chart-sexo")[0].getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["Feminino", "Masculino"],
            datasets: [{
                label: 'Faixa de Sexo',
                data: result[1],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}

function TratarDados(clientes) {

    var zeroToNove =
        dezTodezenove =
        vinteToVNove =
        trintaToTNove =
        quarentaTo =
        sexoM =
        sexoF = 0;
    ko.utils.arrayForEach(clientes(), function (item) {
        if (item.Sexo() == "M") {
            sexoM++;
        }
        if (item.Sexo() == "F") {
            sexoF++;
        }
        if (item.Idade() >= 0 && item.Idade() <= 9) {
            zeroToNove++;
        }
        if (item.Idade() >= 10 && item.Idade() <= 19) {
            dezTodezenove++;
        }
        if (item.Idade() >= 20 && item.Idade() <= 29) {
            vinteToVNove++;
        }
        if (item.Idade() >= 30 && item.Idade() <= 39) {
            trintaToTNove++;
        }
        if (item.Idade() >= 40) {
            quarentaTo++;
        }
    });

    var result = [[zeroToNove,
        dezTodezenove,
        vinteToVNove,
        trintaToTNove,
        quarentaTo], [sexoF, sexoM]]

    return result;
}