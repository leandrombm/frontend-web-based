﻿using CRUDProject.Application.Interfaces;
using CRUDProject.Application.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CRUDProject.Presentation.Controllers
{
    public class HomeController : Controller
    {
        

        private readonly IClienteAppService _clienteAppService;
        public HomeController(IClienteAppService clienteAppService)
        {
            _clienteAppService = clienteAppService;
        }


        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetAll()
        {
            var clientesViewModel = _clienteAppService.GetAll();
            return new JsonResult() { Data = clientesViewModel, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpGet]
        public JsonResult Details(int id)
        {
            var clienteViewModel = _clienteAppService.GetById(id);
            return new JsonResult() { Data = clienteViewModel, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }


        [HttpPost]
        public JsonResult Create(ClienteViewModel clienteViewModel)
        {
            try
            {
                _clienteAppService.Add(clienteViewModel);
                return new JsonResult() { Data = HttpStatusCode.OK, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            catch (Exception)
            {
                return new JsonResult() { Data = HttpStatusCode.NotImplemented, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

        }

       
        // POST: cliente/Edit/5
        [HttpPatch]
        public JsonResult Edit(ClienteViewModel clienteViewModel)
        {
            try
            {
                _clienteAppService.Update(clienteViewModel);
                return new JsonResult() { Data = HttpStatusCode.OK, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            catch (Exception)
            {
                return new JsonResult() { Data = HttpStatusCode.NotImplemented, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }
        [HttpDelete]
        public ActionResult Delete(int id)
        {
            try
            {
                _clienteAppService.Remove(id);
                return new JsonResult() { Data = HttpStatusCode.OK, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            catch (Exception)
            {
                return new JsonResult() { Data = HttpStatusCode.NotImplemented, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }
    }
}
