﻿using CRUDProject.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUDProject.Infra.Data.EntityConfig
{
    public class ClienteConfiguration: EntityTypeConfiguration<Cliente>
    {
        public ClienteConfiguration()
        {
            ToTable("Cliente");

            HasKey(c => c.Id);

            Property(c => c.Id)
                .HasColumnName("cod_cliente");

            Property(c => c.NomeCliente)
                .HasColumnName("nome");

            Property(c => c.Idade)
                .HasColumnName("idade");

            Property(c => c.Endereco)
                .HasColumnName("endereco");

            Property(c => c.Profissao)
              .HasColumnName("profissao");

            Property(c => c.Sexo)
                .HasColumnName("sexo")
                .HasMaxLength(1);


        }
    }
}
