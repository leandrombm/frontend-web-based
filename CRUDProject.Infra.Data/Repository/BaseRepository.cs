﻿using CRUDProject.Domain.Interfaces;
using CRUDProject.Infra.Data.Contexto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUDProject.Infra.Data.Repository
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        protected ContextoCrud DbContexto = new ContextoCrud();
        public void Add(TEntity entity)
        {
            DbContexto.Set<TEntity>().Add(entity);
            DbContexto.SaveChanges();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return DbContexto.Set<TEntity>().ToList();
        }

        public TEntity GetById(int id)
        {
            return DbContexto.Set<TEntity>().Find(id);
        }

        public void Remove(TEntity entity)
        {
            DbContexto.Set<TEntity>().Remove(entity);
            DbContexto.SaveChanges();
        }

        public void Update(TEntity entity)
        {
            DbContexto.Entry(entity).State = System.Data.Entity.EntityState.Modified;
            DbContexto.SaveChanges();
        }
    }
}
