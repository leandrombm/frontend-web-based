﻿using CRUDProject.Domain.Entities;
using CRUDProject.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUDProject.Infra.Data.Repository
{
    public class ClienteRepository:BaseRepository<Cliente>, IClienteRepository
    {
    }
}
