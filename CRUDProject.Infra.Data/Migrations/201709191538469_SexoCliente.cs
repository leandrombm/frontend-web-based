namespace CRUDProject.Infra.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SexoCliente : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cliente", "sexo", c => c.String(maxLength: 1, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cliente", "sexo");
        }
    }
}
