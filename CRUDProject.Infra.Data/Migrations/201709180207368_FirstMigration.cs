namespace CRUDProject.Infra.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cliente",
                c => new
                    {
                        cod_cliente = c.Int(nullable: false, identity: true),
                        nome = c.String(maxLength: 100, unicode: false),
                        idade = c.Int(nullable: false),
                        endereco = c.String(maxLength: 100, unicode: false),
                        profissao = c.String(maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.cod_cliente);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Cliente");
        }
    }
}
