﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUDProject.Domain.Entities
{
    public class Cliente
    {
        public int Id { get; set; }
        public string NomeCliente { get; set; }
        public int Idade { get; set; }
        public string Endereco { get; set; }
        public string Profissao { get; set; }
        public string Sexo { get; set; }

    }
}
