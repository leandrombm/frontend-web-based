﻿using CRUDProject.Application.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUDProject.Application.Interfaces
{
    public interface IClienteAppService: IBaseAppService<ClienteViewModel>
    {
    }
}
