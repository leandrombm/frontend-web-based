﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUDProject.Application.Interfaces
{
    public interface IBaseAppService<TEntityViewModel>: IDisposable
    { 
        void Add(TEntityViewModel entityViewModel);
        void Update(TEntityViewModel entityViewModel);
        IEnumerable<TEntityViewModel> GetAll();
        TEntityViewModel GetById(int id);
        void Remove(int id);
    }
}
